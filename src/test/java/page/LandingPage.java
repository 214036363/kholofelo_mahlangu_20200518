package page;

import org.openqa.selenium.By;

public class LandingPage {

    public static By linkSignUp = By.xpath("//a[contains(text(),'Sign Up')]");
    public static By fieldEmail = By.xpath("//input[@id='email']");
    public static By checkBoxRememberMe = By.xpath("//label[contains(text(),'Remember Me')]");
    public static By btnNext = By.xpath("//body/div[@id='body_wrapper']/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/fieldset[1]/button[1]");
    public static By linkMoreLoginOptions = By.xpath("//span[contains(text(),'More Login Options')]");


}
