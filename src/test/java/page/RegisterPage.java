package page;

import org.openqa.selenium.By;

public class RegisterPage {

    public static By linkLoginHere = By.xpath("//a[contains(text(),'Login Here')]");
    public static By fieldName = By.xpath("//input[@id='userName']");
    public static By fieldEmail = By.xpath("//input[@id='email1']");
    public static By fieldMobileNo = By.xpath("//input[@id='mobile']");
    public static By fieldJobTitle = By.xpath("//input[@id='job']");
    public static By fieldCompanyName = By.xpath("//input[@id='company']");
    public static By checkTerms = By.xpath("//body/div[@id='body_wrapper']/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/fieldset[1]/div[1]/div[1]/div[7]/div[1]/div[1]");
    public static By btnNext = By.xpath("//body/div[@id='body_wrapper']/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/fieldset[1]/div[1]/button[1]");
    public static By emailDuplicateError = By.xpath("//p[contains(text(),'This email address already exists')]");
}
