package test;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.LandingPage;
import page.RegisterPage;
import utility.ElementHandler;
import utility.ExcelReader;
import utility.SeleniumBaseClass;

import java.io.IOException;

public class RegisterExistingUser extends SeleniumBaseClass {



    @Test(dataProvider = "ApplicantData")
    public void runRegisterExistingUser(String name, String email, String job, String company)
    {

            LandingPage landingPage = new LandingPage();
            RegisterPage registerPage = new RegisterPage();
            ElementHandler elementHandler = new ElementHandler(driver);
            elementHandler.clickElement(landingPage.linkSignUp);
            elementHandler.inputData(registerPage.fieldName,name);
            elementHandler.inputData(registerPage.fieldEmail,email);
            elementHandler.inputData(registerPage.fieldMobileNo,elementHandler.generateCelNumber());
            elementHandler.inputData(registerPage.fieldJobTitle,job);
            elementHandler.inputData(registerPage.fieldCompanyName,company);
            elementHandler.clickElement(registerPage.checkTerms);
            elementHandler.clickElement(registerPage.btnNext);
            Assert.assertTrue(elementHandler.assertElementVisibility(registerPage.emailDuplicateError));
            elementHandler.takeScreenShot("RegisterExistingUser");


    }



    @DataProvider(name="ApplicantData")
    public static Object[][] getInvalidCustomer() throws IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        String sheetName = "applicant_info";
        return ExcelReader.readExcel(sheetName);
    }
}
