package utility;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


import java.io.File;
import java.io.IOException;
import java.util.Random;

public class ElementHandler {

    WebDriver driver;

    public ElementHandler(WebDriver driver) {
        this.driver = driver;
    }


    //Method clicks provided element
    public void clickElement(By elementBy)
    {
        if (assertElementVisibility(elementBy))
        {
            driver.findElement(elementBy).click();
        }else {
            Assert.fail("Couldnt find element: "+elementBy);
        }
    }

    //Method to input given data into given field element
    public void inputData(By elementBy,String stringData)
    {
        assertElementVisibility(elementBy);
        driver.findElement(elementBy).clear();
        driver.findElement(elementBy).sendKeys(stringData);
    }

    //Method to generate random phone numbers
    public  String generateCelNumber()
    {
        Random random = new Random();
        int a = random.nextInt(80)+10;
        int b = random.nextInt(99)+100;
        int c = random.nextInt(9999);

        String randomCell = "0".concat(String.valueOf(a))+b+c;
        System.out.println(randomCell);
        return randomCell;
    }

    public boolean assertElementVisibility(By elementBy)
    {
        try{
            WebDriverWait wait = new WebDriverWait(driver,40);
            System.out.println("waiting for element: "+elementBy);
            wait.until(ExpectedConditions.visibilityOf( driver.findElement(elementBy)));
        }catch (Exception exception)
        {

            return false;
        }
        return true;
    }

    public void takeScreenShot(String testName)
    {

        try {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("screenshots/"+testName+".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
