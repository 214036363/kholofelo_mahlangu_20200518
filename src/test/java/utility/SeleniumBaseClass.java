package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;


import java.util.concurrent.TimeUnit;

public class SeleniumBaseClass {
    public static WebDriver driver;

    @BeforeSuite(alwaysRun = true)
    public void setUp(){

        ChromeOptions options = new ChromeOptions();

        options.addArguments("--start-maximized");
        System.setProperty("webdriver.chrome.driver", "WebDrivers/chromedriver_mac");
        System.out.println("==================================================");
        try {
            driver = new ChromeDriver(options);
            System.out.println("Now starting Chrome browser");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);


        driver.navigate().to("https://uatweb.signinghub.co.za");

    }

    @AfterTest
    public void wrapTest()
    {
        System.out.println("Loggin out after test");
    }

    @AfterSuite
    public void fininshUp() throws InterruptedException {
        Thread.sleep(20000);
        driver.close();

    }
}
